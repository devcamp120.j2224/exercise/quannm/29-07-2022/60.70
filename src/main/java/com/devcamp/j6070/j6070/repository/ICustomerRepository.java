package com.devcamp.j6070.j6070.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j6070.j6070.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    CCustomer findById(long customer_id);
}
