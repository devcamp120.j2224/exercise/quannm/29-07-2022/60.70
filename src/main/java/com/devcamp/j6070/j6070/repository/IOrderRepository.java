package com.devcamp.j6070.j6070.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j6070.j6070.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {
    
}
