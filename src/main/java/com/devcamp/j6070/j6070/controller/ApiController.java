package com.devcamp.j6070.j6070.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j6070.j6070.model.CCustomer;
import com.devcamp.j6070.j6070.model.COrder;
import com.devcamp.j6070.j6070.repository.ICustomerRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ApiController {
    @Autowired
    ICustomerRepository pCustomerRepository;
    @GetMapping("/orderById")
    public ResponseEntity<Set<COrder>> getOrderByCustomerId(@RequestParam long customerId) {
        try {
            CCustomer vCustomer = pCustomerRepository.findById(customerId);
            if (vCustomer != null) {
                return new ResponseEntity<Set<COrder>>(vCustomer.getOrders(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
